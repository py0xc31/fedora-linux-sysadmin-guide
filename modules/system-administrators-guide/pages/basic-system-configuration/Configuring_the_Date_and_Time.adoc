
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Configuring_the_Date_and_Time]]
= Configuring the Date and Time

Modern operating systems distinguish between the following two types of clocks:

* A _real-time clock_ (*RTC*), commonly referred to as a _hardware clock_, (typically an integrated circuit on the system board) that is completely independent of the current state of the operating system and runs even when the computer is shut down.

* A _system clock_, also known as a _software clock_, that is maintained by the kernel and its initial value is based on the real-time clock. Once the system is booted and the system clock is initialized, the system clock is completely independent of the real-time clock.

The system time is always kept in _Coordinated Universal Time_ (*UTC*) and converted in applications to local time as needed. _Local time_ is the actual time in your current time zone, taking into account _daylight saving time_ (*DST*). The real-time clock can use either UTC or local time. UTC is recommended.

{MAJOROSVER} offers three command line tools that can be used to configure and display information about the system date and time: the [command]#timedatectl# utility, which is new in {MAJOROSVER} and is part of `systemd`pass:attributes[{blank}]; the traditional [command]#date# command; and the [command]#hwclock# utility for accessing the hardware clock.

[[sect-Configuring_the_Date_and_Time-timedatectl]]
== Using the timedatectl Command

The [application]*timedatectl* utility is distributed as part of the `systemd` system and service manager and allows you to review and change the configuration of the system clock. You can use this tool to change the current date and time, set the time zone, or enable automatic synchronization of the system clock with a remote server.

For information on how to display the current date and time in a custom format, see also xref:Configuring_the_Date_and_Time.adoc#sect-Configuring_the_Date_and_Time-date[Using the date Command].

[[sect-Configuring_the_Date_and_Time-timedatectl-Display]]
=== Displaying the Current Date and Time

To display the current date and time along with detailed information about the configuration of the system and hardware clock, run the [command]#timedatectl# command with no additional command line options:

[subs="quotes, macros"]
----
[command]#timedatectl#
----

This displays the local and universal time, the currently used time zone, the status of the Network Time Protocol (`NTP`) configuration, and additional information related to DST.

[[exam-Configuring_the_Date_and_Time-timedatectl-Display]]
.Displaying the Current Date and Time
====

The following is an example output of the [command]#timedatectl# command on a system that does not use `NTP` to synchronize the system clock with a remote server:

[subs="attributes"]
----
~]${nbsp}timedatectl
      Local time: Mon 2013-09-16 19:30:24 CEST
  Universal time: Mon 2013-09-16 17:30:24 UTC
        Timezone: Europe/Prague (CEST, +0200)
     NTP enabled: no
NTP synchronized: no
 RTC in local TZ: no
      DST active: yes
 Last DST change: DST began at
                  Sun 2013-03-31 01:59:59 CET
                  Sun 2013-03-31 03:00:00 CEST
 Next DST change: DST ends (the clock jumps one hour backwards) at
                  Sun 2013-10-27 02:59:59 CEST
                  Sun 2013-10-27 02:00:00 CET
----

====

[[sect-Configuring_the_Date_and_Time-timedatectl-Time]]
=== Changing the Current Time

To change the current time, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#timedatectl# [option]`set-time` _HH:MM:SS_
----

Replace _HH_ with an hour, _MM_ with a minute, and _SS_ with a second, all typed in two-digit form.

This command updates both the system time and the hardware clock. The result it is similar to using both the [command]#date --set# and [command]#hwclock --systohc# commands.

The command will fail if an `NTP` service is enabled. See xref:Configuring_the_Date_and_Time.adoc#sect-Configuring_the_Date_and_Time-timedatectl-NTP[Synchronizing the System Clock with a Remote Server] to temporally disable the service.

[[exam-Configuring_the_Date_and_Time-timedatectl-Time]]
.Changing the Current Time
====

To change the current time to 11:26 p.m., run the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}timedatectl set-time 23:26:00
----

====

By default, the system is configured to use UTC. To configure your system to maintain the clock in the local time, run the [command]#timedatectl# command with the [option]`set-local-rtc` option as `root`:

[subs="quotes, macros"]
----
[command]#timedatectl# [option]`set-local-rtc` _boolean_
----

To configure your system to maintain the clock in the local time, replace _boolean_ with `yes` (or, alternatively, `y`, `true`, `t`, or `1`). To configure the system to use UTC, replace _boolean_ with `no` (or, alternatively, `n`, `false`, `f`, or `0`). The default option is `no`.

[[sect-Configuring_the_Date_and_Time-timedatectl-Date]]
=== Changing the Current Date

To change the current date, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#timedatectl# [option]`set-time` _YYYY-MM-DD_
----

Replace _YYYY_ with a four-digit year, _MM_ with a two-digit month, and _DD_ with a two-digit day of the month.

Note that changing the date without specifying the current time results in setting the time to 00:00:00.

[[exam-Configuring_the_Date_and_Time-timedatectl-Date]]
.Changing the Current Date
====

To change the current date to 2 June 2013 and keep the current time (11:26 p.m.), run the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}timedatectl set-time "2013-06-02 23:26:00"
----

====

[[sect-Configuring_the_Date_and_Time-timedatectl-Time_Zone]]
=== Changing the Time Zone

To list all available time zones, type the following at a shell prompt:

[subs="quotes, macros"]
----
[command]#timedatectl# [option]`list-timezones`
----

To change the currently used time zone, type as `root`:

[subs="macros"]
----
timedatectl set-timezone pass:quotes[_time_zone_]
----

Replace _time_zone_ with any of the values listed by the [command]#timedatectl list-timezones# command.

[[exam-Configuring_the_Date_and_Time-timedatectl-Time_Zone]]
.Changing the Time Zone
====

To identify which time zone is closest to your present location, use the [command]#timedatectl# command with the [option]`list-timezones` command line option. For example, to list all available time zones in Europe, type:

[subs="macros, attributes"]
----
~]#{nbsp}timedatectl list-timezones | grep Europe
Europe/Amsterdam
Europe/Andorra
Europe/Athens
Europe/Belgrade
Europe/Berlin
Europe/Bratislava
pass:quotes[_…_]
----

To change the time zone to `Europe/Prague`, type as `root`:

[subs="attributes"]
----
~]#{nbsp}timedatectl set-timezone Europe/Prague
----

====

[[sect-Configuring_the_Date_and_Time-timedatectl-NTP]]
=== Synchronizing the System Clock with a Remote Server

As opposed to the manual adjustments described in the previous sections, the [command]#timedatectl# command also allows you to enable automatic synchronization of your system clock with a group of remote servers using the `NTP` protocol. Enabling NTP enables the `chronyd` or `ntpd` service, depending on which of them is installed.

The `NTP` service can be enabled and disabled using a command as follows:

[subs="quotes, macros"]
----
[command]#timedatectl# [option]`set-ntp` _boolean_
----

To enable your system to synchronize the system clock with a remote `NTP` server, replace _boolean_ with `yes` (the default option). To disable this feature, replace _boolean_ with `no`.

[[exam-Configuring_the_Date_and_Time-timedatectl-NTP]]
.Synchronizing the System Clock with a Remote Server
====

To enable automatic synchronization of the system clock with a remote server, type:

[subs="attributes"]
----
~]#{nbsp}timedatectl set-ntp yes
----

The command will fail if an `NTP` service is not installed. See xref:servers/Configuring_NTP_Using_the_chrony_Suite.adoc#sect-Installing_chrony[Installing chrony] for more information.

====

[[sect-Configuring_the_Date_and_Time-date]]
== Using the date Command

The [command]#date# utility is available on all Linux systems and allows you to display and configure the current date and time. It is frequently used in scripts to display detailed information about the system clock in a custom format.

For information on how to change the time zone or enable automatic synchronization of the system clock with a remote server, see xref:Configuring_the_Date_and_Time.adoc#sect-Configuring_the_Date_and_Time-timedatectl[Using the timedatectl Command].

[[sect-Configuring_the_Date_and_Time-date-Display]]
=== Displaying the Current Date and Time

To display the current date and time, run the [command]#date# command with no additional command line options:

[subs="quotes, macros"]
----
[command]#date#
----

This displays the day of the week followed by the current date, local time, abbreviated time zone, and year.

By default, the [command]#date# command displays the local time. To display the time in UTC, run the command with the [option]`--utc` or [option]`-u` command line option:

[subs="quotes, macros"]
----
[command]#date# [option]`--utc`
----

You can also customize the format of the displayed information by providing the [option]`+"pass:attributes[{blank}]_format_pass:attributes[{blank}]"` option on the command line:

----
date +"format"
----

Replace _format_ with one or more supported control sequences as illustrated in xref:Configuring_the_Date_and_Time.adoc#exam-Configuring_the_Date_and_Time-date-Display[Displaying the Current Date and Time]. See xref:Configuring_the_Date_and_Time.adoc#tabl-Configuring_the_Date_and_Time-date-Format[Commonly Used Control Sequences] for a list of the most frequently used formatting options, or the `date`(1) manual page for a complete list of these options.

[[tabl-Configuring_the_Date_and_Time-date-Format]]
.Commonly Used Control Sequences

[options="header"]
|===
|Control Sequence|Description
|[option]`%H`|The hour in the _HH_ format (for example, `17`).
|[option]`%M`|The minute in the _MM_ format (for example, `30`).
|[option]`%S`|The second in the _SS_ format (for example, `24`).
|[option]`%d`|The day of the month in the _DD_ format (for example, `16`).
|[option]`%m`|The month in the _MM_ format (for example, `09`).
|[option]`%Y`|The year in the _YYYY_ format (for example, `2013`).
|`%Z`|The time zone abbreviation (for example, `CEST`).
|[option]`%F`|The full date in the _YYYY-MM-DD_ format (for example, `2013-09-16`). This option is equal to [option]`%Y-%m-%d`.
|[option]`%T`|The full time in the _HH:MM:SS_ format (for example, 17:30:24). This option is equal to [option]`%H:%M:%S`
|===

[[exam-Configuring_the_Date_and_Time-date-Display]]
.Displaying the Current Date and Time
====

To display the current date and local time, type the following at a shell prompt:

[subs="quotes, macros, attributes"]
----
~]${nbsp}pass:attributes[{blank}][command]#date#
Mon Sep 16 17:30:24 CEST 2013
----

To display the current date and time in UTC, type the following at a shell prompt:

[subs="quotes, macros, attributes"]
----
~]${nbsp}pass:attributes[{blank}][command]#date --utc#
Mon Sep 16 15:30:34 UTC 2013
----

To customize the output of the [command]#date# command, type:

[subs="attributes"]
----
~]${nbsp}date +"%Y-%m-%d %H:%M"
2013-09-16 17:30
----

====

[[sect-Configuring_the_Date_and_Time-date-Time]]
=== Changing the Current Time

To change the current time, run the [command]#date# command with the [option]`--set` or [option]`-s` option as `root`:

[subs="quotes, macros"]
----
[command]#date# [option]`--set` _HH:MM:SS_
----

Replace _HH_ with an hour, _MM_ with a minute, and _SS_ with a second, all typed in two-digit form.

By default, the [command]#date# command sets the system clock to the local time. To set the system clock in UTC, run the command with the [option]`--utc` or [option]`-u` command line option:

[subs="quotes, macros"]
----
[command]#date# [option]`--set` _HH:MM:SS_ [option]`--utc`
----

[[exam-Configuring_the_Date_and_Time-date-Time]]
.Changing the Current Time
====

To change the current time to 11:26 p.m., run the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}date --set 23:26:00
----

====

[[sect-Configuring_the_Date_and_Time-date-Date]]
=== Changing the Current Date

To change the current date, run the [command]#date# command with the [option]`--set` or [option]`-s` option as `root`:

[subs="quotes, macros"]
----
[command]#date# [option]`--set` _YYYY-MM-DD_
----

Replace _YYYY_ with a four-digit year, _MM_ with a two-digit month, and _DD_ with a two-digit day of the month.

Note that changing the date without specifying the current time results in setting the time to 00:00:00.

[[exam-Configuring_the_Date_and_Time-date-Date]]
.Changing the Current Date
====

To change the current date to 2 June 2013 and keep the current time (11:26 p.m.), run the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}date --set 2013-06-02 23:26:00
----

====

[[sect-Configuring_the_Date_and_Time-hwclock]]
== Using the hwclock Command

`hwclock` is a utility for accessing the hardware clock, also referred to as the Real Time Clock (RTC). The hardware clock is independent of the operating system you use and works even when the machine is shut down. This utility is used for displaying the time from the hardware clock. `hwclock` also contains facilities for compensating for systematic drift in the hardware clock.

The hardware clock stores the values of: year, month, day, hour, minute, and second. It is not able to store the time standard, local time or Coordinated Universal Time (UTC), nor set the Daylight Saving Time (DST).

The `hwclock` utility saves its settings in the `/etc/adjtime` file, which is created with the first change you make, for example, when you set the time manually or synchronize the hardware clock with the system time.

[NOTE]
====

In {MAJOROS}{nbsp}6, the [command]#hwclock# command was run automatically on every system shutdown or reboot, but it is not in {MAJOROSVER}. When the system clock is synchronized by the Network Time Protocol (NTP) or
Precision Time Protocol (PTP), the kernel automatically synchronizes the hardware clock to the system clock every 11 minutes.

====

For details about NTP, see xref:servers/Configuring_NTP_Using_the_chrony_Suite.adoc#ch-Configuring_NTP_Using_the_chrony_Suite[Configuring NTP Using the chrony Suite] and xref:servers/Configuring_NTP_Using_ntpd.adoc#ch-Configuring_NTP_Using_ntpd[Configuring NTP Using ntpd]. For information about PTP, see xref:servers/Configuring_PTP_Using_ptp4l.adoc#ch-Configuring_PTP_Using_ptp4l[Configuring PTP Using ptp4l]. For information about setting the hardware clock after executing [application]*ntpdate*, see xref:servers/Configuring_NTP_Using_ntpd.adoc#s1-Configuring_the_Hardware_Clock_update[Configuring the Hardware Clock Update].

[[sect2-displaying-time-hwclock]]
=== Displaying the Current Date and Time

Running [command]#hwclock# with no command line options as the `root` user returns the date and time in local time to standard output.

[subs="quotes, macros"]
----
[command]#hwclock#
----

Note that using the [option]`--utc` or [option]`--localtime` options with the [command]#hwclock# command does not mean you are displaying the hardware clock time in UTC or local time. These options are used for setting the hardware clock to keep time in either of them. The time is always displayed in local time. Additionally, using the [command]#hwclock --utc# or [command]#hwclock --local# commands does not change the record in the `/etc/adjtime` file. This command can be useful when you know that the setting saved in `/etc/adjtime` is incorrect but you do not want to change the setting. On the other hand, you may receive misleading information if you use the command an incorrect way. See the `hwclock`(8) manual page for more details.

[[exam-sect3-displaying-time-hwclock]]
.Displaying the Current Date and Time
====

To display the current date and the current local time from the hardware clock, run as `root`:

[subs="attributes"]
----
~]#{nbsp}hwclock
Tue 15 Apr 2014 04:23:46 PM CEST     -0.329272 seconds
----

CEST is a time zone abbreviation and stands for Central European Summer Time.

====

For information on how to change the time zone, see xref:Configuring_the_Date_and_Time.adoc#sect-Configuring_the_Date_and_Time-timedatectl-Time_Zone[Changing the Time Zone].

[[sect3-changing-date-time-hwclock]]
=== Setting the Date and Time

Besides displaying the date and time, you can manually set the hardware clock to a specific time.

When you need to change the hardware clock date and time, you can do so by appending the [option]`--set` and [option]`--date` options along with your specification:

[subs="quotes, macros"]
----
[command]#hwclock --set --date _"dd mmm yyyy HH:MM"_pass:attributes[{blank}]#
----

Replace _dd_ with a day (a two-digit number), _mmm_ with a month (a three-letter abbreviation), _yyyy_ with a year (a four-digit number), _HH_ with an hour (a two-digit number), _MM_ with a minute (a two-digit number).

At the same time, you can also set the hardware clock to keep the time in either UTC or local time by adding the [option]`--utc` or [option]`--localtime` options, respectively. In this case, `UTC` or `LOCAL` is recorded in the `/etc/adjtime` file.

[[exam6-sect3-setting-time-hwclock]]
.Setting the Hardware Clock to a Specific Date and Time
====

If you want to set the date and time to a specific value, for example, to "`21:17, October 21, 2014`", and keep the hardware clock in UTC, run the command as `root` in the following format:

[subs="attributes"]
----
~]#{nbsp}hwclock --set --date "21 Oct 2014 21:17" --utc
----

====

[[sect4-synchronizing-date-time-hwclock]]
=== Synchronizing the Date and Time

You can synchronize the hardware clock and the current system time in both directions.

* Either you can set the hardware clock to the current system time by using this command:
+
[subs="quotes, macros"]
----
[command]#hwclock --systohc#
----
+
Note that if you use NTP, the hardware clock is automatically synchronized to the system clock every 11 minutes, and this command is useful only at boot time to get a reasonable initial system time.

* Or, you can set the system time from the hardware clock by using the following command:
+
[subs="quotes, macros"]
----
[command]#hwclock --hctosys#
----

When you synchronize the hardware clock and the system time, you can also specify whether you want to keep the hardware clock in local time or UTC by adding the [option]`--utc` or [option]`--localtime` option. Similarly to using [option]`--set`, `UTC` or `LOCAL` is recorded in the `/etc/adjtime` file.

The [command]#hwclock --systohc --utc# command is functionally similar to [command]#timedatectl set-local-rtc false# and the [command]#hwclock --systohc --local# command is an alternative to [command]#timedatectl set-local-rtc true#.

[[exam4-sect4-synchornizing-systohc-hwclock]]
.Synchronizing the Hardware Clock with System Time
====

To set the hardware clock to the current system time and keep the hardware clock in local time, run the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}hwclock --systohc --localtime
----

To avoid problems with time zone and DST switching, it is recommended to keep the hardware clock in UTC. The shown xref:Configuring_the_Date_and_Time.adoc#exam4-sect4-synchornizing-systohc-hwclock[Synchronizing the Hardware Clock with System Time] is useful, for example, in case of a multi boot with a Windows system, which assumes the hardware clock runs in local time by default, and all other systems need to accommodate to it by using local time as well. It may also be needed with a virtual machine; if the virtual hardware clock provided by the host is running in local time, the guest system needs to be configured to use local time, too.

====

[[sect-Date_and_Time-Resources]]
== Additional Resources

For more information on how to configure the date and time in {MAJOROSVER}, see the resources listed below.

.Installed Documentation

* `timedatectl`(1) — The manual page for the [command]#timedatectl# command line utility documents how to use this tool to query and change the system clock and its settings.

* `date`(1) — The manual page for the [command]#date# command provides a complete list of supported command line options.

* `hwclock`(8) — The manual page for the [command]#hwclock# command provides a complete list of supported command line options.

.See Also

* xref:basic-system-configuration/System_Locale_and_Keyboard_Configuration.adoc#ch-System_Locale_and_Keyboard_Configuration[System Locale and Keyboard Configuration] documents how to configure the keyboard layout.
